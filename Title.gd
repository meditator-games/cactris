extends Node

const LOGO = [
	"### ###             # ###",
	"#     #      #        #  ",
	"#   ### ### ### ### # ###",
	"#   # # #    #  #   #   #",
	"### ### ###  ## #   # ###"
	]

var _timer = 0

func _ready():
	Global.TitleMusic.play()
	$Box/Margin/Panel/Box/StartButton.grab_focus()
	$Box/Logo/Text.display_blocks(LOGO)
	$Box/Logo/Text/Shadow/ShadowVP/Text.display_blocks(LOGO)

func _process(delta):
	var Shadow = get_node("Box/Logo/Text/Shadow")
	Shadow.offset.x = abs(sin(_timer * 2.0) * 32)
	Shadow.offset.y = Shadow.offset.x
	_timer = fmod(_timer + delta, PI * 2)

func _on_StartButton_pressed():
	Global.TitleMusic.stop()
	get_tree().change_scene("res://Game.tscn")

func _on_HelpButton_pressed():
	get_tree().change_scene("res://Help.tscn")

func _on_CreditsButton_pressed():
	get_tree().change_scene("res://Credits.tscn")

func _on_QuitButton_pressed():
	get_tree().quit()
