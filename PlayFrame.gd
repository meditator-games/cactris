extends Node

onready var Playfield: Node2D = $VBox/Frame/Playfield
var _start_timer = 2.5

func _reposition():
	var ps = Playfield.get_size()
	var scale = $VBox/Frame.rect_size.y / ps.y
	$VBox/Frame.rect_min_size.x = ps.x * scale
	Playfield.scale = Vector2(scale, scale)
	
func _ready():
	_reposition()
	get_tree().connect("screen_resized", self, "_reposition")

func _process(delta):
	$VBox/Score.text = str(Playfield.score)
	$VBox/Frame/Paused.visible = Playfield.paused
	$VBox/Frame/GameOver.visible = Playfield.dead
	$VBox/Frame/Ready.visible = _start_timer > 1.5
	$VBox/Frame/Go.visible = _start_timer <= 1.5 and _start_timer > 0
	if _start_timer > 0:
		_start_timer -= delta
		if _start_timer <= 0:
			Playfield.frozen = false
	
func get_size():
	return Playfield.get_size()

func _on_ReturnButton_pressed():
	get_tree().quit()
