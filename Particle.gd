extends Sprite

var tint: Color setget set_tint

func set_tint(c: Color):
	tint = c
	material.set_shader_param("hue", c.r)
	material.set_shader_param("saturation", c.g)
	material.set_shader_param("value", c.b)
	
func _ready():
	$VP/Spawner.restart()
	$Timer.start($VP/Spawner.lifetime)
	
func _process(delta):
	modulate.a = $Timer.time_left / $VP/Spawner.lifetime

func _on_Timer_timeout():
	queue_free()
