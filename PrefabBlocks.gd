tool
extends Node2D
	
const Playfield = preload("res://Playfield.gd")

func display_blocks(blocks: Array):
	var cells = []
	var height = blocks.size()
	var width = blocks[0].length()
	var positions = []
	for y in range(height):
		var row = []
		cells.append(row)
		for x in range(width):
			var block = null
			match blocks[y][x]:
				"0":
					block = Playfield.Block.instance()
				"+":
					block = Playfield.Cactus.instance()
					block.tint = Color(0, 0, 0)
				"#":
					block = Playfield.Cactus.instance()
					block.tint = Color(0.45, 0, -0.45)
				"B":
					block = Playfield.Bomb.instance()
			if block != null:
				block.position = Vector2(x, y) * Playfield.CELL_SIZE
				add_child(block)
			row.append(block)
			positions.append(Vector2(x, y))
				
	for y in range(height):
		for x in range(width):
			if cells[y][x] == null:
				continue
			if cells[y][x] is preload("res://Cactus.gd"):
				var neighbors = {}
				var p = Vector2(x, y)
				if x > 0:
					neighbors["w"] = p + Vector2(-1, 0)
				if x < width - 1:
					neighbors["e"] = p + Vector2(1, 0)
				if y > 0:
					neighbors["n"] = p + Vector2(0, -1)
				if y < height - 1:
					neighbors["s"] = p + Vector2(0, 1)
				for dir in neighbors.keys():
					var np = neighbors[dir]
					if cells[np.y][np.x] != null and Playfield.Logic.match_tiling(cells[p.y][p.x], cells[np.y][np.x]):
						cells[p.y][p.x].grow(dir)
	Playfield.Logic.update_sprites(cells, [], positions)