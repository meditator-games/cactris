extends Node2D

const Logic = preload("res://Logic.gd")

export var tint: Color setget set_tint
var group = null

func _ready():
	set_tint(tint)

func _notification(what: int):
	if what == NOTIFICATION_PREDELETE:
		if group != null:
			group.erase(self)

func is_alive():
	return true

func get_particle_color():
	return tint
	
func set_tint(c: Color):
	tint = c
	if is_inside_tree():
		$Offset/NW.material.set_shader_param("hue", c.r - 0.45)
		$Offset/NW.material.set_shader_param("saturation", c.g)
		$Offset/NW.material.set_shader_param("value", c.b + 0.45)

func set_offset(delta: Vector2):
	$Offset.position = delta

# this only allows shrinking
func _set_sprite(sprite: AnimatedSprite, v: bool, h: bool):
	if v and h:
		pass
	elif v:
		if  sprite.animation == "both":
			sprite.animation = "vert"
	elif h:
		if sprite.animation == "both":
			sprite.animation = "horiz"
	else:
		if sprite.animation == "horiz-horiz":
			sprite.animation = "horiz-corner"
		elif sprite.animation != "horiz-corner":
			sprite.animation = "corner"

func _set_vertical(sprite: AnimatedSprite):
	if sprite.animation == "horiz" or sprite.animation == "horiz-horiz":
		sprite.animation = "both"
	elif sprite.animation == "corner" or sprite.animation == "horiz-corner":
		sprite.animation = "vert"
		
func _unset_horizontal(sprite: AnimatedSprite):
	if sprite.animation == "horiz-horiz":
		sprite.animation = "horiz"
	elif sprite.animation == "horiz-corner":
		sprite.animation = "corner"
		
func _set_horizontal(sprite: AnimatedSprite):
	if sprite.animation == "horiz-corner":
		sprite.animation = "horiz-horiz"
	elif sprite.animation == "corner":
		sprite.animation = "horiz"
	elif sprite.animation == "vert":
		sprite.animation = "both"
		
func _try_convert_to_horizontal():
	if $Offset/NE.animation == "corner" and $Offset/NW.animation == "corner" and $Offset/SE.animation == "corner" and $Offset/SE.animation == "corner":
		$Offset/NE.animation = "horiz-corner"
		$Offset/NW.animation = "horiz-corner"
		$Offset/SE.animation = "horiz-corner"
		$Offset/SW.animation = "horiz-corner"

func grow(dir: String):
	match dir:
		"n":
			_set_vertical($Offset/NW)
			_set_vertical($Offset/NE)
			_unset_horizontal($Offset/SW)
			_unset_horizontal($Offset/SE)
		"e":
			_try_convert_to_horizontal()
			_set_horizontal($Offset/NE)
			_set_horizontal($Offset/SE)
		"s":
			_unset_horizontal($Offset/NW)
			_unset_horizontal($Offset/NE)
			_set_vertical($Offset/SW)
			_set_vertical($Offset/SE)
		"w":
			_try_convert_to_horizontal()
			_set_horizontal($Offset/NW)
			_set_horizontal($Offset/SW)

func recalc_neighbors(neighbors: Dictionary):
	var n: bool = neighbors.has("n") and Logic.match_tiling(neighbors["n"], self)
	var e: bool = neighbors.has("e") and Logic.match_tiling(neighbors["e"], self)
	var s: bool = neighbors.has("s") and Logic.match_tiling(neighbors["s"], self)
	var w: bool = neighbors.has("w") and Logic.match_tiling(neighbors["w"], self)
	
	_set_sprite($Offset/NW, n, w)
	_set_sprite($Offset/NE, n, e)
	_set_sprite($Offset/SW, s, w)
	_set_sprite($Offset/SE, s, e)