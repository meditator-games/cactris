# Cactris

A ouzzle game about growing cactuses. Made with Godot. Code is GPLv3 licensed, art is CC-BY-SA. Press Start 2P font licensed under the OFL. See licenses folder for more info.
