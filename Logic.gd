extends Node



static func match_scoring(a, b):
	return a.tint != null and b.tint != null and a.tint == b.tint

static func match_tiling(a, b):
	return a.get_script() == b.get_script() and a.tint == b.tint
	
static func get_tagged_neighbors(cells: Array, p: Vector2, include_diagonal: bool = true) -> Dictionary:
	var height = cells.size()
	var width = cells[0].size()
	var neighbors = {}
	if p.x > 0:
		neighbors["w"] = p + Vector2(-1, 0)
	if p.x < width - 1:
		neighbors["e"] = p + Vector2(1, 0)
	if p.y > 0:
		neighbors["n"] = p + Vector2(0, -1)
	if p.y < height - 1:
		neighbors["s"] = p + Vector2(0, 1)
		
	if include_diagonal:
		if neighbors.has("n") and neighbors.has("w"):
			neighbors["nw"] = p + Vector2(-1, -1)
		if neighbors.has("n") and neighbors.has("e"):
			neighbors["ne"] = p + Vector2(1, -1)
		if neighbors.has("s") and neighbors.has("w"):
			neighbors["sw"] = p + Vector2(-1, 1)
		if neighbors.has("s") and neighbors.has("e"):
			neighbors["se"] = p + Vector2(1, 1)
	return neighbors
	
static func get_tagged_blocks(cells: Array, pos: Vector2, tags: Dictionary, falling: Array) -> Dictionary:
	var cur = cells[pos.y][pos.x]
	var blocks = {}
	if cur != null:
		for tag in tags.keys():
			var p = tags[tag]
			var block = cells[p.y][p.x]
			if block != null and falling.has(cur) == falling.has(block):
				blocks[tag] = block
	return blocks
	
static func update_sprites(cells: Array, falling: Array, blocks: Array):
	var updated = {}
	for p in blocks:
		var block = cells[p.y][p.x]
		var neighbors = get_tagged_neighbors(cells, p)
		updated[p] = get_tagged_blocks(cells, p, neighbors, falling)
		for np in neighbors.values():
			if not updated.has(np) and cells[np.y][np.x] != null:
				updated[np] = get_tagged_blocks(cells, np, get_tagged_neighbors(cells, np), falling)
	for p in updated.keys():
		var block = cells[p.y][p.x]
		if block != null:
			block.recalc_neighbors(updated[p])