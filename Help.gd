extends Control

func _ready():
	$Margin/Panel/Box/Line2/Blocks/Preview.display_blocks(["00  0+", " 00 +0"])
	$Margin/Panel/Box/Line3/Blocks/Preview.display_blocks(["# #", "###"])

func _on_ReturnButton_pressed():
	get_tree().change_scene("res://Title.tscn")