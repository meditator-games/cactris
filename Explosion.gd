extends Particles2D

func _ready():
	restart()
	$Timer.start(lifetime)

func _on_Timer_timeout():
	queue_free()
