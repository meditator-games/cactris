tool
extends Node2D

const Logic = preload("res://Logic.gd")

export var tint: Color setget set_tint

func is_alive():
	return false
	
func get_particle_color():
	return tint

func set_tint(c: Color):
	tint = c
	$Offset/NW.material.set_shader_param("hue", c.r)
	$Offset/NW.material.set_shader_param("saturation", c.g)
	$Offset/NW.material.set_shader_param("value", c.b)

func set_offset(delta: Vector2):
	$Offset.position = delta
	
func _set_sprite(sprite, v, h, vh):
	if v and h:
		if vh:
			sprite.animation = "fill"
		else:
			sprite.animation = "both"
	elif v:
		sprite.animation = "vert"
	elif h:
		sprite.animation = "horiz"
	else:
		sprite.animation = "corner"

func recalc_neighbors(neighbors: Dictionary):
	var n: bool = neighbors.has("n") and Logic.match_tiling(neighbors["n"], self)
	var e: bool = neighbors.has("e") and Logic.match_tiling(neighbors["e"], self)
	var s: bool = neighbors.has("s") and Logic.match_tiling(neighbors["s"], self)
	var w: bool = neighbors.has("w") and Logic.match_tiling(neighbors["w"], self)
	var nw: bool = neighbors.has("nw") and Logic.match_tiling(neighbors["nw"], self)
	var ne: bool = neighbors.has("ne") and Logic.match_tiling(neighbors["ne"], self)
	var sw: bool = neighbors.has("sw") and Logic.match_tiling(neighbors["sw"], self)
	var se: bool = neighbors.has("se") and Logic.match_tiling(neighbors["se"], self)
	
	_set_sprite($Offset/NW, n, w, nw)
	_set_sprite($Offset/NE, n, e, ne)
	_set_sprite($Offset/SW, s, w, sw)
	_set_sprite($Offset/SE, s, e, se)