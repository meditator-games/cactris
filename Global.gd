extends Node

var current_scene = null
onready var TitleMusic = $TitleMusic

func _enter_tree():
	var root = get_tree().root
	current_scene = root.get_child(root.get_child_count() -1)
	get_tree().current_scene = current_scene
	
func goto_scene(scene: Node):
	current_scene.free()
	current_scene = scene
	get_tree().root.add_child(current_scene)
	get_tree().current_scene = current_scene
