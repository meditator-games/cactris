extends Node2D

const MATCH_SIZE = 5                 # number of blocks that need to match to clear
const BOMB_CHANCE = 1/20.0           # chance per block that a bomb will spawn
const CACTUS_CHANCE = 1/4.0          # chance per block that a cactus will spawn
const GROWTH_RATE = 1                # increase to allow cactuses to grow longer
const DEFAULT_SIZE = Vector2(13, 13) # change the default map size

const COLORS = [Color(0, 0, 0), Color(1/6.0, 0, 0), Color(2/6.0, 0, 0), Color(3/6.0, 0, 0), Color(4/6.0, 0, 0), Color(5/6.0, 0, 0)]
const CELL_SIZE = 64
const BLINKS = 8
const MIN_SPEED = 1.0
const MAX_SPEED = 8.0
const SPEED_TICK = 60.0
const DROP_SPEED = 1 / 20.0
const NEXT_QUEUE_SIZE = 3
const ANIM_FACTOR = 1.0/8.0
const FALL_FACTOR = 1.0/8.0
const GUIDE_ALPHA = 0.1

const Block = preload("res://Block.tscn")
const Bomb = preload("res://Bomb.tscn")
const BombClass = preload("res://Bomb.gd")
const Cactus = preload("res://Cactus.tscn")
const SpawnX = preload("res://SpawnX.tscn")

const Logic = preload("res://Logic.gd")

var _spawn_point: Vector2 = Vector2()
var _width = 0
var _height = 0
var _cells: Array = [] # playfield grid
var _current = null
var _guide = null
var _spawn_marks: Dictionary = {}
var _last_current = null
var _current_midpoint = null
var _game_over_timer: float = 0
var _speed: float = MIN_SPEED
var _anim_speed: float = _speed
var _current_fall_timer: float = 0
var _current_anim_timer: float = 0
var _drop_timer: float = 0
var _clear_timer: float = 0
var _clearing: Array = []
var _fall_timer: float = 0
var _grow_timer: float = 0
var _growing: Array = []
var _falling: Array = []
var _landed: Array = []
var _chain: Array = [] # array of array of colors matched - used only for scoring
var _next: Array = [] # list of pieces in the queue
var _alive_blocks: Array = [] # blocks that have a potential action after a piece drops
var _swap: QueuedPiece = null
var _swapped: bool = false
var _play_timer: float = 0
var paused: bool = false
var frozen: bool = true
var score: int = 0
var dead: bool = false

func reset(size: Vector2, puzzle: Dictionary = {}):
	randomize()
	self._width = size.x
	self._height = size.y
	$BG.scale = size
	$BG.texture_scale = size
	
	self._spawn_point = Vector2(floor(_width / 2 - 1), 0)
	self.dead = false
	self.score = 0
	self._current = null
	self._guide = null
	self._last_current = null
	self._current_midpoint = null
	self._game_over_timer = 0
	self._speed = MIN_SPEED
	self._anim_speed = _speed
	self._current_fall_timer = 0
	self._current_anim_timer = 0
	self._drop_timer = 0
	self._clear_timer = 0
	self._clearing = []
	self._fall_timer = 0
	self._grow_timer = 0
	self._growing = []
	self._falling = []
	self._landed = []
	self._chain = []
	self._alive_blocks = []
	self._swap = null
	self._swapped = false
	self._play_timer = 0
	
	for block in $Pieces.get_children():
		block.queue_free()
	for block in $Next.get_children():
		block.queue_free()
		
	self._cells = []
	for y in range(_height):
		var row: Array = []
		row.resize(_width)
		self._cells.append(row)
	_next = []
	while _next.size() < NEXT_QUEUE_SIZE:
		_queue_piece()
		
	for p in puzzle.keys():
		var block = [Block, Cactus, Bomb][puzzle[p][0]].instance()
		block.tint = puzzle[p][1]
		block.position = p * CELL_SIZE
		$Pieces.add_child(block)
		_cells[p.y][p.x] = block
	Logic.update_sprites(_cells, [], puzzle.keys())
	
func get_size() -> Vector2:
	return ($BG.scale + Vector2(3, 0)) * CELL_SIZE

# can be used to add default blocks
"""const PUZZLE = {
	Vector2( 2, 7): [1, COLORS[0]],
	Vector2( 3, 7): [1, COLORS[0]],
	Vector2( 4, 7): [1, COLORS[0]],
	Vector2( 5, 7): [1, COLORS[0]],
	Vector2( 6, 7): [1, COLORS[0]],
	Vector2( 7, 7): [1, COLORS[0]],
	Vector2( 2, 12): [2, COLORS[0]],
	Vector2( 2, 11): [1, COLORS[3]],
	Vector2( 2, 10): [0, COLORS[3]],
	Vector2( 2,  9): [0, COLORS[3]],
	Vector2( 2,  8): [0, COLORS[2]],
	Vector2( 3, 12): [0, COLORS[1]],
	Vector2( 3, 11): [2, COLORS[1]],
	Vector2( 3, 10): [0, COLORS[4]],
	Vector2( 3,  9): [0, COLORS[4]],
	Vector2( 3,  8): [0, COLORS[2]]
}"""

func _ready():
	reset(DEFAULT_SIZE) # , PUZZLE)
		
func start_game_over():
	if not dead:
		dead = true
		_game_over_timer = 5

func _move_blocks(blocks: Array, midpoint: Vector2, transform: Transform2D):
	var moved = []
	var pieces = []
	var blocked: bool = false
	
	for p in blocks:
		var dest = transform.xform(p - midpoint).round() + midpoint
		moved.append(dest)
		pieces.append(_cells[p.y][p.x])
		
	for i in range(blocks.size()):
		var dest = moved[i]
		if not (blocks.has(dest) or (dest.x >= 0 and dest.x < _width and dest.y >= 0 and dest.y < _height and _cells[dest.y][dest.x] == null)):
			blocked = true
	if blocked:
		if transform.get_rotation() != 0 and transform.get_origin().length_squared() == 0:
			for delta in [Vector2(-1, 0), Vector2(1, 0), Vector2(0, 1), Vector2(0, -1)]:
				var kicked = _move_blocks(blocks, midpoint, transform.translated(delta))
				if kicked != null:
					return kicked
		return null
		
	for i in range(blocks.size()):
		var p = blocks[i]
		_cells[p.y][p.x] = null
	for i in range(blocks.size()):
		var p = moved[i]
		_cells[p.y][p.x] = pieces[i]
		pieces[i].position = p * CELL_SIZE
	return moved

func _move_current(transform: Transform2D) -> bool:
	if _current == null:
		return false
	var moved = _move_blocks(_current, _current_midpoint, transform)
	if moved:
		_last_current = _current
		_current = moved
		_current_midpoint += transform.origin
		_current_anim_timer = ANIM_FACTOR / _anim_speed
		_reposition_guide()
		return true
	return false

func _is_blocked(blocks: Array, offset: Vector2) -> bool:
	if blocks == null:
		return false
	for p in blocks:
		var test = p + offset
		if not blocks.has(test) and (test.x < 0 or test.y < 0 or test.x >= _width or test.y >= _height or _cells[test.y][test.x] != null):
			return true
	return false

func _try_extend_current_fall_time(delta):
	if _is_blocked(_current, Vector2(0, 1)):
		_current_fall_timer += delta

func _reset_current_fall_timer():
	_speed = clamp(round(_play_timer / SPEED_TICK), MIN_SPEED, MAX_SPEED)
	_anim_speed = sqrt(sqrt(_speed))
	_current_fall_timer = 1 / _speed

func _get_neighbors(p):
	var neighbors = []
	if p.x > 0:
		neighbors.append(p + Vector2(-1, 0))
	if p.x < _width - 1:
		neighbors.append(p + Vector2(1, 0))
	if p.y > 0:
		neighbors.append(p + Vector2(0, -1))
	if p.y < _height - 1:
		neighbors.append(p + Vector2(0, 1))
	return neighbors

func _search_matches(pos: Vector2, group: Array = [], visited: Dictionary = {}) -> Array:
	if visited.has(pos):
		return group
	var block = _cells[pos.y][pos.x]
	if block == null:
		return group
	visited[pos] = true
	group.append(pos)
	for n in _get_neighbors(pos):
		var nb = _cells[n.y][n.x]
		if nb != null and Logic.match_scoring(nb, block):
			_search_matches(n, group, visited)
	return group

func _spawn_particles(type: PackedScene, pos: Vector2):
	var particle = type.instance()
	particle.position = (pos + Vector2(0.5, 0.5)) * CELL_SIZE
	$Particles.add_child(particle)
	return particle

func _spawn_explosion(pos: Vector2) -> Node2D:
	return _spawn_particles(preload("res://Explosion.tscn"), pos)
	
func _spawn_block_particles(pos: Vector2, tint: Color):
	var particle = _spawn_particles(preload("res://Particle.tscn"), pos)
	particle.tint = tint
	return particle


func _try_fall(blocks, force: bool = false):
	_falling.clear()
	var new_landed = []
	for pos in blocks:
		var p = pos
		var block = _cells[p.y][p.x]
		if block != null and block.is_alive():
			if not _alive_blocks.has(block):
				_alive_blocks.append(block)
			if force:
				_landed.append(p)
		else: 
			var dest = p + Vector2(0, 1)
			if dest.y < _height and _cells[dest.y][dest.x] == null:
				var above = _cells[p.y][p.x]
				while above != null and p.y >= 0 and not above.is_alive():
					_falling.append(p)
					p += Vector2(0, -1)
					above = _cells[p.y][p.x]
			if not _falling.has(p):
				_landed.append(p)
				new_landed.append(p)
	
	for p in new_landed:
		var block = _cells[p.y][p.x]
		if block != null and not (block is BombClass):
			_spawn_block_particles(p, block.get_particle_color())
	Logic.update_sprites(_cells, _falling, new_landed)

	if _falling.empty():
		if not _landed.empty():
			$Thud.play()
		if _check_matches(_landed).empty():
			_chain.clear()
			_start_grow()
			if _grow_timer <= 0:
				_reset_current_fall_timer()
		_landed.clear()
	else:
		_fall_timer = FALL_FACTOR / _anim_speed

func _piece_landed():
	for p in _current:
		_cells[p.y][p.x].set_offset(Vector2())
	for block in _guide:
		block.queue_free()
	_try_fall(_current, true)
	$Thud.play()
	_guide = null
	_current = null
	_last_current = null

func _check_matches(blocks):
	var groups = []
	for p in blocks:
		var dupe = false
		for group in groups:
			if group.has(p):
				dupe = true
				break
		if not dupe:
			var group = _search_matches(p)
			groups.append(group)
	var cleared = []
	var cur_chain_size = _chain.size() + 1
	for group in groups:
		if group.size() >= MATCH_SIZE:
			_clear_cells(group)
			score += group.size() * cur_chain_size
			cleared.append(group)
	if not cleared.empty():
		$Score.play()
	var bombs = []
	for p in _clearing:
		var ns = Logic.get_tagged_neighbors(_cells, p, false)
		for np in ns.values():
			var block = _cells[np.y][np.x]
			if block != null and block is BombClass and not bombs.has(np):
				bombs.append(np)
				_clearing.append(np)
	return cleared

func _clear_cells(cells: Array):
	_clear_timer = 1 / _anim_speed
	var chain_group = []
	for p in cells:
		chain_group.append(_cells[p.y][p.x].tint)
		_clearing.append(p)
	_chain.append(chain_group)

class QueuedPiece extends Node2D:
	var cells: Dictionary = {}
	var target = null
	
	func _init():
		modulate.a = 0
	
	func _physics_process(delta):
		if modulate.a < 1:
			modulate.a += 1 / 30.0
		if target != null and target != position:
			position = position + (target - position) * 0.1
			
	func add_block(block: Node2D, pos: Vector2):
		block.position = pos * CELL_SIZE
		cells[pos] = block
		add_child(block)

func _gen_block():
	var block
	if randf() < BOMB_CHANCE:
		block = Bomb.instance()
	else:
		if randf() < CACTUS_CHANCE:
			block = Cactus.instance()
			block.group = [block]
		else:
			block = Block.instance()
		block.tint = COLORS[randi() % COLORS.size()]
	return block

func _queue_piece():
	var new_next = QueuedPiece.new()
	for y in range(2):
		new_next.add_block(_gen_block(), Vector2(0, y))
	new_next.add_block(_gen_block(), Vector2(1, 0))
	$Next.add_child(new_next)
	_next.push_back(new_next)
	_reposition_queue()

func _reposition_queue():
	for y in range(_next.size()):
		if _next[y].target == null:
			_next[y].position = Vector2(_width + 1, (y + 1) * 3) * CELL_SIZE
		_next[y].target = Vector2(_width + 1, y * 3) * CELL_SIZE

func _is_next_piece_ready():
	return _current == null and _clearing.empty() and _falling.empty()

func _reposition_guide():
	var moved = _current.duplicate()
	var blocked = false
	while not blocked:
		for i in range(moved.size()):
			moved[i].y += 1
			var p = moved[i]
			if not _current.has(p) and (p.y >= _height or _cells[p.y][p.x] != null):
				blocked = true
	for i in range(moved.size()):
		_guide[i].position = (moved[i] + Vector2(0, -1)) * CELL_SIZE

func _spawn_piece(piece: QueuedPiece):
	_current = []
	_last_current = _current
	_current_midpoint = Vector2()
	_guide = []
	for np in piece.cells.keys():
		var p = np + _spawn_point
		_current.append(p)
		_current_midpoint += p
		if _cells[p.y][p.x] != null:
			start_game_over()
		var block: Node2D = piece.cells[np]
		piece.remove_child(block)
		$Pieces.add_child(block)
		_cells[p.y][p.x] = block
		block.position = p * CELL_SIZE
		
		var guide_block: Node2D = block.duplicate()
		guide_block.z_index = 8
		guide_block.modulate.a = GUIDE_ALPHA
		$Pieces.add_child(guide_block)
		_guide.append(guide_block)
	piece.queue_free()
	_current_midpoint = (_current_midpoint / _current.size()).round()
	_reset_current_fall_timer()
	_reposition_guide()

func _refresh_spawn_marks():
	for mark in _spawn_marks.values():
		mark.queue_free()
	_spawn_marks.clear()
	var piece = _next[0]
	for np in piece.cells.keys():
		var p = np + _spawn_point
		var mark = SpawnX.instance()
		mark.position = p * CELL_SIZE
		$Marks.add_child(mark)
		_spawn_marks[p] = mark
	
func _start_grow():
	for block in _alive_blocks:
		var pos = block.position / CELL_SIZE
		var neighbors = Logic.get_tagged_neighbors(_cells, pos, false)
		neighbors.erase("s")
		
		for np in neighbors.values():
			if _cells[np.y][np.x] == null and not _spawn_marks.has(np) and randf() < 1/float(block.group.size()):
				_growing.append(pos)
				_grow_timer = 1
				break
	return not _growing.empty()
	
static func get_dir(delta: Vector2) -> String:
	match delta.normalized():
		Vector2(0, -1):
			return "n"
		Vector2(1, 0):
			return "e"
		Vector2(0, 1):
			return "s"
		Vector2(-1, 0):
			return "w"
	return ""

func _do_grow():
	$Grow.play()
	for pos in _growing:
		var neighbors = Logic.get_tagged_neighbors(_cells, pos, false)
		neighbors.erase("s")
		
		var targets = []
		for np in neighbors.values():
			if _cells[np.y][np.x] == null and not _spawn_marks.has(np):
				targets.append(np)
				
		if not targets.empty():
			var new_pos = targets[randi() % targets.size()]
			var block = _cells[pos.y][pos.x]
			var new_block = Cactus.instance()
			new_block.group = block.group
			block.group.append(new_block)
			new_block.position = new_pos * CELL_SIZE
			new_block.tint = block.tint
			_cells[new_pos.y][new_pos.x] = new_block
			$Pieces.add_child(new_block)
			block.grow(get_dir(new_pos - pos))
			new_block.grow(get_dir(pos - new_pos))
			_alive_blocks.append(new_block)
			_spawn_block_particles(new_pos, new_block.get_particle_color())
	_growing.clear()

const EXPLOSION_OFFSETS = [
                                  Vector2( 0, -2),
                 Vector2(-1, -1), Vector2( 0, -1), Vector2( 1, -1),
 Vector2(-2, 0), Vector2(-1,  0),                  Vector2( 1,  0), Vector2( 2,  0),
                 Vector2(-1,  1), Vector2( 0,  1), Vector2( 1,  1),
                                  Vector2( 0,  2)]

func _get_exploded(pos: Vector2):
	var explosions = []
	for offset in EXPLOSION_OFFSETS:
		var dest = pos + offset
		if dest.x >= 0 and dest.y >= 0 and dest.x < _width and dest.y < _height:
			explosions.append(dest)
	return explosions

func _physics_process(delta):
	if frozen:
		return
	if dead:
		for action in ["ui_cancel", "ui_confirm", "ui_select", "pause", "rotate_left", "rotate_right"]:
			if Input.is_action_just_pressed(action):
				#reset(DEFAULT_SIZE)
				get_tree().reload_current_scene()
				break
		return
	if not $Music.playing:
		$Music.play()
	if Input.is_action_just_pressed("pause") or Input.is_action_just_pressed("ui_cancel"):
		paused = not paused
		get_tree().paused = paused
		$Pieces.visible = not paused
		$Marks.visible = $Pieces.visible
		$Next.visible = $Pieces.visible
		$Particles.visible = $Pieces.visible
	if paused:
		return
	_play_timer += delta
	if _clear_timer >= 0:
		_clear_timer -= delta
	elif _fall_timer >= 0:
		_fall_timer -= delta
	elif _grow_timer > 0:
		_grow_timer -= delta
		if _grow_timer < 0.5 and not _growing.empty():
			_do_grow()
	elif  _current_fall_timer >= 0:
		_current_fall_timer -= delta
	
	if _fall_timer >= 0:
		for p in _falling:
			_cells[p.y][p.x].set_offset(Vector2(0, (1 - _fall_timer / (FALL_FACTOR / _anim_speed)) * CELL_SIZE))
	elif not _falling.empty():
		for p in _falling:
			_cells[p.y][p.x].set_offset(Vector2())
		var moved = _move_blocks(_falling, Vector2(), Transform2D(0, Vector2(0, 1)))
		_try_fall(moved)
		
	if _clear_timer >= 0:
		for p in _clearing:
			_cells[p.y][p.x].visible = int(_clear_timer / (1 / _anim_speed) * BLINKS) % 2 == 0
	elif not _clearing.empty():
		var above = []
		var explosions: Dictionary = {}
		var removed: Dictionary = {}
			
		# remove all the bombs and add exploded tiles to removed
		while not _clearing.empty():
			var p = _clearing.pop_front()
			if not removed.has(p):
				removed[p] = true
				var block = _cells[p.y][p.x]
				if block is BombClass:
					explosions[p] = true
					for ep in _get_exploded(p):
						explosions[ep] = true
						if not removed.has(ep):
							_clearing.append(ep)
					block.queue_free()
					_cells[p.y][p.x] = null
		
		if explosions.empty():
			$Clear.play()
		else:
			$Explode.play()
		
		# go through all matched tiles and explosions
		for p in removed.keys():
			var block = _cells[p.y][p.x]
			if explosions.has(p):
				_spawn_explosion(p)
			elif block != null:
				_spawn_block_particles(p, block.get_particle_color())
				
			if block != null:
				var a = p + Vector2(0, -1)
				if a.y >= 0:
					var above_block = _cells[a.y][a.x]
					if above_block != null and not above_block.is_alive() and not removed.has(a):
						above.append(a)
				_alive_blocks.erase(block)
				block.queue_free()
				_cells[p.y][p.x] = null
				
		var explosion_neighbors = {}
		for p in removed.keys():
			for np in Logic.get_tagged_neighbors(_cells, p).values():
				if _cells[np.y][np.x] != null:
					explosion_neighbors[np] = true
				
		Logic.update_sprites(_cells, [], explosion_neighbors.keys())
				
		_try_fall(above)
		
	if _is_next_piece_ready() and _current_fall_timer < 0:
		if true:
			_spawn_piece(_next.pop_front())
			_queue_piece()
			_refresh_spawn_marks()
		else:
			# spawn rocks/cactus?
			pass
	if _current != null and not dead:
		if Input.is_action_just_pressed("move_left"):
			if _move_current(Transform2D(0, Vector2(-1, 0))):
				$Move.play()
				_try_extend_current_fall_time(delta)
		if Input.is_action_just_pressed("move_right"):
			if _move_current(Transform2D(0, Vector2(1, 0))):
				$Move.play()
				_try_extend_current_fall_time(delta)
		if Input.is_action_just_pressed("rotate_left"):
			if _move_current(Transform2D(PI * -0.5, Vector2(0, 0))):
				$Move.play()
				_try_extend_current_fall_time(delta)
		if Input.is_action_just_pressed("rotate_right"):
			if _move_current(Transform2D(PI * 0.5, Vector2(0, 0))):
				$Move.play()
				_try_extend_current_fall_time(delta)
			
		if Input.is_action_just_pressed("drop_instant"):
			while _move_current(Transform2D(0, Vector2(0, 1))):
				pass
			_piece_landed()
		elif Input.is_action_just_pressed("swap") and not _swapped:
			_swapped = true
		else:
			if Input.is_action_just_pressed("drop"):
				_drop_timer = 0
			elif Input.is_action_pressed("drop"):
				_drop_timer -= delta
			if (Input.is_action_pressed("drop") and _drop_timer <= 0) or  _current_fall_timer < 0:
				_reset_current_fall_timer()
				if _move_current(Transform2D(0, Vector2(0, 1))):
					$Move.play()
				else:
					_piece_landed()
			if Input.is_action_pressed("drop") and _drop_timer <= 0:
				_drop_timer += DROP_SPEED
			
	if _current != null:
		if  _current_anim_timer >= 0:
			_current_anim_timer -= delta
		for i in range(_current.size()):
			var p = _current[i]
			var last = _last_current[i]
			var block = _cells[p.y][p.x]
			var t =  clamp(_current_anim_timer / (ANIM_FACTOR / _anim_speed), 0, 1)
			block.set_offset(((last - p) * CELL_SIZE) * t * t)
			